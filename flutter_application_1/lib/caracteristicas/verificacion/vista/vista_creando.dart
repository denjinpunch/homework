import 'package:flutter/material.dart';

class VistaEsperandoNombre extends StatelessWidget {
  const VistaEsperandoNombre({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text('Dame el nombre del doggo'),
        const TextField(),
        TextButton(onPressed: () {}, child: const Text('xd'))
      ],
    );
  }
}
